# WebService JENI

Penambahan fitur pencarian peserta pada WebService JENI

## Fitur Baru

* `Informasi status peserta` - Menampilkan nama, jenis kelamin, tanggal lahir, status, jenis peserta, kelas, dan nama faskes peserta.
* `Informasi status anggot keluarga` - Menampilkan nama, jenis kelamin, tanggal lahir, status, jenis peserta, kelas, dan nama faskes anggota keluarga.
* `Informasi nomor virtual account` - Menampilkan nomor virtual account peserta.
* `Informasi iuran dan status pembayaran peserta` - Menampilkan iuran dan status pembayaran peserta.
