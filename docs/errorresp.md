# Error Documentation

## Expired Service

Generate signature lebih dari 5 menit.

`response`
```json
{
    "metaData": {
        "message": "Expired Service",
        "code": "401"
    }
}
```

---

## Signature Service Tidak Sesuai

Signature hasil dari ConsID dan ConsPwd tidak sesuai.

`response`
```json
{
    "metaData": {
        "message": "Signature Service Tidak Sesuai",
        "code": "401"
    }
}
```

---

## Parameter Tidak Valid

Parameter yang dikirim tidak berjumlah 13 atau 16 digit.

`response`
```json
{
   "metaData": {
       "code": "301",
       "message": "Noka / NIK tidak tidak lengkap atau salah"
   },
   "response": null
}
```

---

## Parameter Bukan Angka

Parameter yang dikirim bukan angka.

`response`
```json
{
   "metaData": {
       "code": "306",
       "message": "Noka / NIK harus angka"
   },
   "response": null
}
```

---

## Jenis Peserta Selain PBPU atau Bukan Pekerja

Response jenis peserta dari service `informasi nomor virtual account` dan `informasi iuran dan status pembayaran` bukan PBPU atau BP.

`response`
```json
{
   "metaData": {
       "code": "304",
       "message": "Jenis Kepesertaan Bukan PBPU atau BP"
   },
   "response": null
}
```

---

## Peserta Tidak Terdaftar

Peserta tidak terdaftar di MasterFile Kepesertaan BPJS Kesehatan

`response`
```json
{
   "metaData": {
       "code": "302",
       "message": "Peserta Tidak Terdaftar"
   },
   "response": null
}
```

---

## Melebihi Limit Akses Service

Service sudah melebihi limit akses per hari.

`response`
```json
{
   "metaData": {
       "code": "305",
       "message": "Telah melewati batas akses untuk fitur ini"
   },
   "response": null
}
```