# Katalog

## Informasi Status Peserta 

Menampilkan nama, jenis kelamin, tanggal lahir, status, jenis peserta, kelas, dan nama faskes peserta.

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/kepesertaan/cekstatus/(parameter)"
```

```
parameter harus numeric atau angka; parameter harus berjumlah 13 atau 16 digit
```

`success response`
```json
{
    "metaData": {
        "code": "200",
        "message": "OK"
    },
    "response": {
        "nmPeserta": "MUHAMMADDIN FAISAL",
        "jnskelPeserta": "Laki-laki",
        "tglLhrPeserta": "1977-08-21",
        "nmStatusPeserta": "Aktif",
        "jnsPeserta": "PEKERJA MANDIRI",
        "klsPeserta": "3",
        "nmPPK": "PUSKESMAS CIPUTAT"
    }
}

```

---

## Informasi Status Anggota Keluarga

Menampilkan nama, jenis kelamin, tanggal lahir, status, jenis peserta, kelas, dan nama faskes anggota keluarga.

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/kepesertaan/cekkeluarga/(parameter)"
```

```
parameter harus numeric atau angka; parameter harus berjumlah 13 atau 16 digit
```

`success response`
```json
{
   "metaData": {
       "code": "200",
       "message": "Ok"
   },
   "response": {
       "list": [
           {
               "nokapstKepalaKeluarga": "0001270533699",
               "nmKepalaKeluarga": "MUHAMMADDIN FAISAL",
               "jnskelKepalaKeluarga": "Laki-laki",
               "nmHubKelKepalaKeluarga": "PESERTA",
               "nmPPKKepalaKeluarga": "PUSKESMAS CIPUTAT",
               "nokapstAgtKeluarga": "0001270533699",
               "nmAgtKeluarga": "MUHAMMADDIN FAISAL",
               "jnskelAgtKeluarga": "Laki-laki",
               "nmHubKelAgtKeluarga": "PESERTA",
               "nmPPKAgtKeluarga": "PUSKESMAS CIPUTAT",
               "nmStatusAgtKeluarga": "Aktif"
           },
           {
               "nokapstKepalaKeluarga": "0001270533699",
               "nmKepalaKeluarga": "MUHAMMADDIN FAISAL",
               "jnskelKepalaKeluarga": "Laki-laki",
               "nmHubKelKepalaKeluarga": "PESERTA",
               "nmPPKKepalaKeluarga": "PUSKESMAS CIPUTAT",
               "nokapstAgtKeluarga": "0001492955223",
               "nmAgtKeluarga": "JOUKEY",
               "jnskelAgtKeluarga": "Laki-laki",
               "nmHubKelAgtKeluarga": "ANAK",
               "nmPPKAgtKeluarga": "PUSKESMAS CIPUTAT",
               "nmStatusAgtKeluarga": "Aktif"
           },
           {
               "nokapstKepalaKeluarga": "0001270533699",
               "nmKepalaKeluarga": "MUHAMMADDIN FAISAL",
               "jnskelKepalaKeluarga": "Laki-laki",
               "nmHubKelKepalaKeluarga": "PESERTA",
               "nmPPKKepalaKeluarga": "PUSKESMAS CIPUTAT",
               "nokapstAgtKeluarga": "0001492955256",
               "nmAgtKeluarga": "JOUKEYE",
               "jnskelAgtKeluarga": "Laki-laki",
               "nmHubKelAgtKeluarga": "ANAK",
               "nmPPKAgtKeluarga": "PUSKESMAS CIPUTAT",
               "nmStatusAgtKeluarga": "Aktif"
           }
       ]
   }
}
```

---

## Informasi Nomor Virtual Account

Menampilkan nomor virtual account peserta.

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/kepesertaan/cekva/(parameter)"
```

```
parameter harus numeric atau angka; parameter harus berjumlah 13 atau 16 digit
```

`success response`
```json
{
   "metaData": {
       "code": "200",
       "message": "Ok"
   },
   "response": {
       "noVA": "8888801270533699"
   }
}
```

---

## Informasi Iuran dan Status Pembayaran Peserta 

Menampilkan iuran dan status pembayaran peserta

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/iuran/cekstatus/(parameter)"
```

```
parameter harus numeric atau angka; parameter harus berjumlah 13 atau 16 digit
```

`success response`
```json
{
   "metaData": {
       "code": "200",
       "message": "Ok"
   },
   "response": {
       "premi": "0",
       "status": "Lunas"
   }
}
```

---

## Informasi Status Tiket

Menampilkan status tiket

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/tiket/status/(parameter)"
```

```
parameter adalah nomor tiket
```

`success response`
```json
{
    "metaData": {
        "code": "200",
        "message": "OK"
    },
    "response": {
        "kodetiket": "K.09-20190312094739",
        "judultiket": "BAYI APBN LAHIR 2019 TIDAK BISA SIMPAN DATA",
        "deskripsi": "Selamat siang,  Mohon bantuannya bayi APBN lahir 2019 (0002742956932 ) tidak bisa simpan data . Pada tanggal 29/01/2019 dilakukan penginputan bayi APBN tetapi terjadi kesalahan pada penginputan tahun lahir bayi, di input tahun lahir 2018 sedangkan bayi lahir 25/01/2019. inquery telah di lakukan memakai NIK pada akte kelahiran bayi tetapi pada saat di simpan notif yang muncul adalah  &#34;Tahun Lahir Bayi Baru Lahir Tidak bisa diubah&#34;    terima kasih",
        "aplikasi": "Kepesertaan",
        "kategori": "APLIKASI",
        "subkategori": "Aktivasi Peserta",
        "tanggalbuat": "12 Mar 2019 09:47:39",
        "status": "Closed",
        "catatan": "Terkait dengan perubahan identitas untuk bayi pbi apbn akan mengacu pada inquery NIK via aplikasi Kepesertaan\nsaat ini sedang dilakukan optimalisasi agar fitur tersebut diakomdir pada aplikasi kepesertaan, \nuntuk optimalisasi aplikasi apabila telah selesai dilakukan, akan disampaikan melalui ITHD\ndemikian disampaikan\nsalam\nPKTI",
        "posisitiket": "KeluhanTI (OTI)",
        "user": "Natalia Ona Paruak",
        "unitkerja": "KPP (Cabang Makasar)",
        "email": "natalia.ona@bpjs-kesehatan.go.id"
    }
}
```

---

## Informasi Eskalasi Tiket

Menampilkan riwayat eskalasi tiket

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/tiket/eskalasi/(parameter)"
```

```
parameter adalah nomor tiket
```

`success response`
```json
{
    "metaData": {
        "code": "200",
        "message": "Ok"
    },
    "response": {
        "list": [
            {
                "kodetiket": "P.04-20181206220718",
                "dari": "KeluhanTI (OTI)",
                "ke": "Deputi Direksi Bidang Pengembangan Sistem Informasi",
                "catataneskalasi": "Yth. Kedeputian Bidang PSI\nBersama ini kami sampaikan bahwa dari Kedeputian Bidang Perluasan Kepesertaan sudah membuat surat nomor 85/PERSER/0119 perihal Penyesuaian Referensi KC pada Aplikasi Portal BPJS tanggal 15 Januari 2018 kepada Kedeputian PSI sebagai tindaklanjut atas tiket ini terkait Kantor Cabang Pemekaran yang belum update pada Aplikasi Portal BPJS.\nAtas Kerjasama yang baik, kami ucapkan terima kasih.",
                "tanggaleskalasi": "15 Jan 2019 16:49:03"
            },
            {
                "kodetiket": "P.04-20181206220718",
                "dari": "Deputi Direksi Bidang Perluasan Kepesertaan",
                "ke": "KeluhanTI (OTI)",
                "catataneskalasi": "Yth. Service Desk\n\nBersama ini kami sampaikan surat nomor 85/PERSER/0119 perihal Penyesuaian Referensi KC pada Aplikasi Portal BPJS tanggal 15 Januari 2018 kepada Kedeputian PSI sebagai tindaklanjut atas tiket P.04-20181206220718 terkait Kantor Cabang Pemekaran yang belum update pada Aplikasi Portal BPJS.\n\nDemikian disampaikan, terima kasih.",
                "tanggaleskalasi": "15 Jan 2019 16:46:13"
            },
            {
                "kodetiket": "P.04-20181206220718",
                "dari": "KeluhanTI (OTI)",
                "ke": "Deputi Direksi Bidang Perluasan Kepesertaan",
                "catataneskalasi": "Mohon untuk dilakukan pengecekan terkait referensinya KC Cibinong, KC TIgaraksa dan KC Cikarang karena tidak ditemukan di portal bpjs.go.id\n\nAtas perhatian dan kerjasama yang baik diucapkan Terima Kasih. \nSalam PKTI",
                "tanggaleskalasi": "07 Dec 2018 11:20:59"
            },
            {
                "kodetiket": "P.04-20181206220718",
                "dari": "Asisten Deputi Bidang Monitoring dan Evaluasi (Deputi Wilayah DKI Jakarta, Bogor, Depok, Tangerang dan Bekasi)",
                "ke": "KeluhanTI (OTI)",
                "catataneskalasi": "yth Asdep Bidang penanganan keluhan ti, mohon bantuan dan arahanya, terima kasih",
                "tanggaleskalasi": "07 Dec 2018 11:07:07"
            },
            {
                "kodetiket": "P.04-20181206220718",
                "dari": "ITHD Cabang Tigaraksa (Cabang Tigaraksa)",
                "ke": "Asisten Deputi Bidang Monitoring dan Evaluasi (Deputi Wilayah DKI Jakarta, Bogor, Depok, Tangerang dan Bekasi)",
                "catataneskalasi": "Mohon bantuan TeamServiceDesk",
                "tanggaleskalasi": "07 Dec 2018 10:58:53"
            },
            {
                "kodetiket": "P.04-20181206220718",
                "dari": "Kepala Bidang Perluasan Peserta dan Kepatuhan (Cabang Tigaraksa)",
                "ke": "ITHD Cabang Tigaraksa (Cabang Tigaraksa)",
                "catataneskalasi": "Tiket Berhasil Dibuat",
                "tanggaleskalasi": "06 Dec 2018 22:07:18"
            }
        ]
    }
}
```

---

## Informasi Penanganan Tiket

Menampilkan riwayat penanganan tiket

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/tiket/penanganan/(parameter)"
```

```
parameter adalah nomor tiket
```

`success response`
```json
{
    "metaData": {
        "code": "200",
        "message": "Ok"
    },
    "response": {
        "list": [
            {
                "kodetiket": "E.00-20160215160255",
                "user": "Operasional Teknologi Informasi",
                "unitkerja": "Aditya Putra Perdana",
                "deskripsipenanganan": "sip saya mengerti",
                "tanggalpenanganan": "15 Feb 2016 17:27:56"
            },
            {
                "kodetiket": "E.00-20160215160255",
                "user": "Operasional Teknologi Informasi",
                "unitkerja": "Aditya Putra Perdana",
                "deskripsipenanganan": "tes saya bingung",
                "tanggalpenanganan": "15 Feb 2016 16:22:36"
            },
            {
                "kodetiket": "E.00-20160215160255",
                "user": "PKTI (SPKTI)",
                "unitkerja": "Rizka Marsa Pramadani",
                "deskripsipenanganan": "mohonn....",
                "tanggalpenanganan": "15 Feb 2016 16:10:05"
            }
        ]
    }
}
```